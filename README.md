---------------
TESSELLATED TERRAIN WITH DYNAMIC LOD
----------------
CAP 5707 Project -- 12/16/14
Victor Bushong

---------------
REQUIREMENTS
----------------
Requirments to compile and run the projects:

    Visual Studio 2013
	An OpenGL 4.2+ compatible GPU

---------------
SOURCES
----------------
There are two source samples included:

    "src_uniform"      :  a uniform patch terrain implementation.
    "src_non_uniform"  :  a non-uniform patch terrain implementation.
	
Inside each source directory is a folder named "visualstudio" which
contains the Visual Studio solution file (.sln) for the project.
With the solution open, simply compile and run the project like normal.

The "data" directory contains scene files, textures, and terrain data 
(heightmaps). These are shared between the two source examples. Scene
files prefixed with "nu_" are configured for non-uniform patch scenes; scene
files configured with "u_" are configured for uniform patch scenes.

---------------	
COMMANDS
---------------

The mouse is used to move throughout the scene. The following list tells what
functions are performed when the mouse buttons are held down and the 
mouse is moved:

    MOUSE BUTTONS
    Right         : Look around
    Left          : Move mouse forward/backward to move forward/backward
	              : Move mouse left/right to look left/right
    Left + Right  : Move mouse forward/backward to move up/down
	              : Move mouse left/right to move left/right

Here are a list of keyboard commands:

	KEY   : FUNCTION
	w     : Move forward
	s     : Move backward
	a     : Move left
	d     : Move right
	e     : Move up (non-uniform implementation only)
	q     : Move down (non-uniform implementation only)
	p     : Turn on wireframe overlay
	o     : Turn off wireframe overlay
	u     : Toggle skybox on/off (non-uniform implementation only)