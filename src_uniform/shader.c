#include <stdio.h>
#include "GL/glus.h"
#include "shared.h"

GLuint g_ubo_matrixBlock;
GLint g_ubo_matrixBlock_offsets[4];

GLuint g_ubo_lightBlock;
GLint g_ubo_lightBlock_offsets[2];



//
// Tessellation program
//
GLUSprogram g_program;
GLuint g_program_vertexAttribute;
GLuint g_program_patchTexCoordAttribute;

void shader_programInit()
{
	GLUStextfile vert, tcs, tes, gs, frag;

	if (!glusFileLoadText("../shader/tess.vert.glsl", &vert)) printf("Error loading shader source: %s\n", "../shader/tess.vert.glsl");
	if (!glusFileLoadText("../shader/tess.tcs.glsl", &tcs)) printf("Error loading shader source: %s\n", "../shader/tess.tcs.glsl");
	if (!glusFileLoadText("../shader/tess.tes.glsl", &tes)) printf("Error loading shader source: %s\n", "../shader/tess.tes.glsl");
	if (!glusFileLoadText("../shader/tess.gs.glsl", &gs)) printf("Error loading shader source: %s\n", "../shader/tess.gs.glsl");
	if (!glusFileLoadText("../shader/tess.frag.glsl", &frag)) printf("Error loading shader source: %s\n", "../shader/tess.frag.glsl");

	if (!glusProgramBuildFromSource(&g_program, 
		(const GLUSchar**)&vert.text, 
		(const GLUSchar**)&tcs.text,
		(const GLUSchar**)&tes.text,
		(const GLUSchar**)&gs.text, 
		(const GLUSchar**)&frag.text))
		printf("Error compiling shader program.\n");

	glusFileDestroyText(&vert);
	glusFileDestroyText(&tcs);
	glusFileDestroyText(&tes);
	glusFileDestroyText(&gs);
	glusFileDestroyText(&frag);

	glUseProgram(g_program.program);
	
	//
	// Matrix block UBO
	//

	//// Find the matrix block
	//GLuint blockIndex = glGetUniformBlockIndex(g_program.program, "MatrixBlock");

	//// Allocate temporary buffer
	//GLint blockSize;
	//glGetActiveUniformBlockiv(g_program.program, blockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &blockSize);
	//GLubyte * blockBuffer;
	//blockBuffer = (GLubyte*)malloc(blockSize);

	//// Get offsets for the parts of the block
	//const GLchar *names[] = { "mMatrix", "pMatrix", "mvMatrix", "mvpMatrix", "nMatrix" };
	//GLuint indices[5];
	//glGetUniformIndices(g_program.program, 5, names, indices);
	//glGetActiveUniformsiv(g_program.program, 5, indices, GL_UNIFORM_OFFSET, g_ubo_matrixBlock_offsets);

	//// Load temp buffer with zeroes
	//memset(blockBuffer, 0, blockSize);

	//// Create the UBO
	//glGenBuffers(1, &g_ubo_matrixBlock);
	//glBindBuffer(GL_UNIFORM_BUFFER, g_ubo_matrixBlock);
	//glBufferData(GL_UNIFORM_BUFFER, blockSize, blockBuffer, GL_DYNAMIC_DRAW);

	//// Bind the buffer to the shader in the program
	//// (the index parameter is the binding layout specified in the shader)
	//glBindBufferBase(GL_UNIFORM_BUFFER, 0, g_ubo_matrixBlock);

	//// done with that
	//free(blockBuffer);

	//
	// Light block UBO
	//

	//// Find the matrix block
	//blockIndex = glGetUniformBlockIndex(g_program.program, "LightBlock");

	//// Allocate temporary buffer
	//glGetActiveUniformBlockiv(g_program.program, blockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &blockSize);
	//blockBuffer = (GLubyte*)malloc(blockSize);

	//// Get offsets for the parts of the block
	//const GLchar *names2[] = { "LightOrigin", "LightColor" };
	//GLuint indices2[2];
	//glGetUniformIndices(g_program.program, 2, names2, indices2);
	//glGetActiveUniformsiv(g_program.program, 2, indices2, GL_UNIFORM_OFFSET, g_ubo_lightBlock_offsets);

	//// Load temp buffer with zeroes
	//memset(blockBuffer, 0, blockSize);

	//// Create the UBO
	//glGenBuffers(1, &g_ubo_lightBlock);
	//glBindBuffer(GL_UNIFORM_BUFFER, g_ubo_lightBlock);
	//glBufferData(GL_UNIFORM_BUFFER, blockSize, blockBuffer, GL_DYNAMIC_DRAW);

	//// Bind the buffer to the shader in the program
	//// (the index parameter is the binding layout specified in the shader)
	//glBindBufferBase(GL_UNIFORM_BUFFER, 1, g_ubo_lightBlock);

	//// done with that
	//free(blockBuffer);

	//
	// Setup attributes
	//
	g_program_vertexAttribute = glGetAttribLocation(g_program.program, "a_vertex");
	g_program_patchTexCoordAttribute = glGetAttribLocation(g_program.program, "a_patchTexCoord");
}

//void shader_updateMatrixBlock(int offsetIndex, GLsizeiptr size, const void *data)
//{
//	glBindBuffer(GL_UNIFORM_BUFFER, g_ubo_matrixBlock);
//	glBufferSubData(GL_UNIFORM_BUFFER, g_ubo_matrixBlock_offsets[offsetIndex], size, data);
//}
//
//void shader_updateLightBlock(int offsetIndex, GLsizeiptr size, const void *data)
//{
//	glBindBuffer(GL_UNIFORM_BUFFER, g_ubo_lightBlock);
//	glBufferSubData(GL_UNIFORM_BUFFER, g_ubo_lightBlock_offsets[offsetIndex], size, data);
//}