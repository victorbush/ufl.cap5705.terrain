void skybox_render();
void skybox_init();

extern GLuint g_vbo_skybox; // Vertex buffer object
extern GLuint g_vao_skybox; // Vertex attribute object
extern GLuint g_ibo_skybox; // Index buffer object