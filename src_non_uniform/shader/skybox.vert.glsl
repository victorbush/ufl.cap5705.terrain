#version 420

//
// Uniforms
//
uniform	mat4 pMatrix;
uniform	mat4 mvMatrix;

//
// Inputs
//
in vec4 a_vertex;

//
// Outputs
//
out vec3 vs_texCoord;

void main(void)
{
	gl_Position = pMatrix * mvMatrix * a_vertex;
	vs_texCoord = normalize(a_vertex.xyz) * -1;
}